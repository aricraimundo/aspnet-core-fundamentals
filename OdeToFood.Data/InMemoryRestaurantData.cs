#nullable disable
using OdeToFood.Core;

namespace OdeToFood.Data;

public class InMemoryRestaurantData : IRestaurantData
{
    List<Restaurant> _restaurants;

    public InMemoryRestaurantData()
    {
        this._restaurants = new List<Restaurant>() {
            new Restaurant { Id = 1, Name = "Scott's Pizza", Location = "West 32rd Street", Cuisine = CuisineType.Italian },
            new Restaurant { Id = 2, Name = "Dino's Pizza", Location = "1234 Header Drive", Cuisine = CuisineType.Mexican },
            new Restaurant { Id = 3, Name = "Helio's Pizza", Location = "2nd Street Avenue", Cuisine = CuisineType.Indian }
        };
    }

    public IEnumerable<Restaurant> GetRestaurantsByName(string name)
    {
        return from r in this._restaurants
               where string.IsNullOrEmpty(name) || r.Name.StartsWith(name)
               orderby r.Name
               select r;
    }

    public Restaurant GetById(int id)
    {
        return this._restaurants.SingleOrDefault(r => r.Id == id);
    }

    public Restaurant Update(Restaurant updatedRestaurant)
    {
        var restaurant = this._restaurants.SingleOrDefault(r => r.Id == updatedRestaurant.Id);

        if (restaurant != null)
        {
            restaurant.Name = updatedRestaurant.Name;
            restaurant.Location = updatedRestaurant.Location;
            restaurant.Cuisine = updatedRestaurant.Cuisine;
        }

        return restaurant;
    }

    public Restaurant Add(Restaurant newRestaurant)
    {
        this._restaurants.Add(newRestaurant);
        newRestaurant.Id = this._restaurants.Max(r => r.Id) + 1;
        return newRestaurant;
    }

    public int Commit()
    {
        return 0;
    }

    public Restaurant Delete(int id)
    {
        var restaurant = this._restaurants.FirstOrDefault(r => r.Id == id);
        if (restaurant != null)
        {
            this._restaurants.Remove(restaurant);
        }

        return restaurant;
    }

    public int GetCountOfRestaurants()
    {
        return this._restaurants.Count();
    }
}