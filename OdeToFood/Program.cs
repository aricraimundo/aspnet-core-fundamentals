using OdeToFood.Data;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddControllers();
builder.Services.AddScoped<IRestaurantData, SqlRestaurantData>();
builder.Services.AddDbContextPool<OdeToFoodDbContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("OdeToFoodDb"));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

// Running database migrations
/*
if (!app.Environment.IsDevelopment())
{
    using (var scope = app.Services.CreateScope())
    {
        var db = scope.ServiceProvider.GetRequiredService<OdeToFoodDbContext>();
        db.Database.Migrate();
    }
}
*/

app.Use(async (ctx, next) =>
{
    if (ctx.Request.Path.StartsWithSegments("/hello"))
    {
        await ctx.Response.WriteAsync("Hello, World!");
    }
    else
    {
        await next(ctx);
    }
});

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseNodeModules();
app.UseRouting();
app.UseAuthorization();

app.MapRazorPages();
app.MapControllers();

app.Run();
